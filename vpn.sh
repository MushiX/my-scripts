#!/bin/bash

#Usage:
#	sudo bash vpn.sh update
#	sudo bash vpn.sh pl16 udp

#Sprawdzanie czy w aktualnej konfiguracji resolv.conf [DNS] jest słowo "domain"
STATUS=`cat /etc/resolv.conf | grep domain`

if [ "$1" == "update" ]
then
	cd /etc/openvpn
	wget https://downloads.nordcdn.com/configs/archives/servers/ovpn.zip
	rm -r ovpn_tcp
	rm -r ovpn_udp
	unzip ovpn.zip
	rm ovpn.zip
	folder="/etc/openvpn/ovpn_tcp/"
	for file in "$folder"*
	do
		sed -i 's/auth-user-pass/auth-user-pass auth.txt/' $file
	done
	folder="/etc/openvpn/ovpn_udp/"
	for file in "$folder"*
	do
		sed -i 's/auth-user-pass/auth-user-pass auth.txt/' $file
	done
elif [ "$#" == 1 ]; then
	cd /etc/openvpn/ovpn_udp/
	best=9999
	for file in $1*; do
		if [ "${file:14:1}" == "m" ]; then
			new=`ping -c 2 "${file:0:15}" | awk -F '/' 'END {print $5}'`
			if (($(bc <<< "$best > $new"))); then
				echo ${file:0:3} $new
				best=$new
			fi
		elif [ "${file:15:1}" == "m" ]; then
			new=`ping -c 2 "${file:0:16}" | awk -F '/' 'END {print $5}'`
			if (($(bc <<< "$best > $new"))); then
				echo ${file:0:4} $new
				best=$new
			fi
		else
			new=`ping -c 2 "${file:0:17}" | awk -F '/' 'END {print $5}'`
			if (($(bc <<< "$best > $new"))); then
				echo ${file:0:5} $new
				best=$new
			fi
		fi
	done
else
	if [ "$STATUS" != '' ]
	then											#Przełączanie na NordVPN
		echo "Przełączenie na NordVPN: " $1
		echo "#NordVPN
nameserver 176.103.130.130
nameserver 176.103.130.131" > /etc/resolv.conf
		cd /etc/openvpn
		openvpn --config /etc/openvpn/ovpn_$2/$1.nordvpn.com.$2.ovpn --daemon
	else											#Przełączanie na Vectrę
		echo "Przełączanie na Vectrę"
		sudo kill openvpn
		echo "#Vectra
domain luban.vectranet.pl
nameserver 176.103.130.130
nameserver 176.103.130.131" > /etc/resolv.conf
	fi
fi

#NordVPN Oryginal DNS
#103.86.96.100
#103.86.99.100

#Normal Vectra DNS
#domain luban.vectranet.pl
#nameserver 37.8.214.2
#nameserver 31.11.202.254